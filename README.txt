This module provides a text field to enter a South African ID.
The ID number can then be validated.

There is also a API function southafrican_id_validate_id($id) that other modules
can use to validate a ID number.
The function returns TRUE or FALSE to specify validity of number.

This module is only for Drupal 7

TODO:

Extract date of birth from number
Extract gender from number
Extract citizen type from number
